package com.sdust.mall;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

// ValueOperations：简单K-V操作
@SpringBootTest
public class TestValue {
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void setValue() {
        redisTemplate.opsForValue().set("age", 23);
        redisTemplate.opsForValue().set("name", "赵四");
    }

    @Test
    public void getValue() {
        String name = (String) redisTemplate.opsForValue().get("name");
        System.out.println(name);
    }

    @Test
    public void deleteValue() {
        redisTemplate.delete("name");
    }
}