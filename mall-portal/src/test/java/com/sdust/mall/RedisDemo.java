package com.sdust.mall;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;

public class RedisDemo {

    @Test
    public void testJedis() {
        // 1.new Jedis对象 连接Redis
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        // 2.操作Redis
        String name = jedis.get("name");
        System.out.println(name);
        jedis.set("name", name+"_new");
        name = jedis.get("name");
        System.out.println(name);
        // 3.关闭Redis
        jedis.close();
    }
}
