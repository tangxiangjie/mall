package com.sdust.mall.interceptor;

import com.sdust.mall.pojo.entity.User;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import java.io.IOException;

// 拦截器，定义后需要在WebConfig类中配置一下
// 拦截需要登录的 页面&方法 的访问请求，
// 比如从加购的post请求中  请求加购的同时附带一个returnUri，如果已登录，则用不到；未登录，作为参数拼接到跳转登录页面的后边
// login.html页面有js代码获取地址栏中returnUri，不为空，则登录后跳转到returnUri
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            // /page/product/list
            // /page/cart/list
            // /cart/add?returnUri=/page/product/detail?id='+id
            String uri = request.getRequestURI();
            String returnUri = request.getParameter("returnUri");
            // 没有附带的returnUri  说明不是被拦截后发的AJAX请求(我自己的页面我知道，逻辑闭环) 而是地址栏输入，直接带着并重定向就行了-->
            if (ObjectUtils.isEmpty(returnUri)) {
                response.sendRedirect("/page/login?returnUri=" + uri);
            } else { // 附带returnUri的AJAX请求，需要进一步处理，见函数
                redirect(request, response);
            }
            return false;//拦截
        }
        return true;//放行
    }

    private void redirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String returnUri = request.getParameter("returnUri");
        //获取当前请求的路径
        //logger.error("请求类型:"+request.getHeader("X-Requested-With"));
        response.setHeader("Access-Control-Expose-Headers", "REDIRECT,CONTEXTPATH");
        //告诉ajax我是重定向
        response.setHeader("REDIRECT", "REDIRECT");
        //告诉ajax我重定向的路径
        String url = "https://xxx.xxx.xxx/";//重定向路径
        response.setHeader("CONTEXTPATH", "/page/login?returnUri=" + returnUri);
        response.getWriter().write(1);
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    }
}
