package com.sdust.mall.controller.dto;

import lombok.Data;

@Data
public class AliPay {
    private String subject;
    private String traceNo;
    private String totalAmount;
    private String returnUrl;
}
