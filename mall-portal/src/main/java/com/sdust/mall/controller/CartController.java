package com.sdust.mall.controller;

import com.sdust.mall.pojo.entity.Cart;
import com.sdust.mall.pojo.entity.User;
import com.sdust.mall.pojo.query.CartQuery;
import com.sdust.mall.pojo.vo.CartVO;
import com.sdust.mall.service.ICartService;
import com.sdust.mall.util.Result;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private ICartService cartService;

    @RequestMapping("/add")
    public Result add(Cart record, HttpSession session) {
        User user = (User) session.getAttribute("user");
        record.setUserId(user.getId());
        record.setChecked(1);
        record.setQuantity(1);
        cartService.add(record);
        return Result.ok("添加成功");
    }

    @RequestMapping("/list")
    public Result list(CartQuery cartQuery, HttpSession session) {
        User user = (User) session.getAttribute("user");
        // CartQuery cartQuery = new CartQuery();
        cartQuery.setUserId(user.getId());
        List<CartVO> cartRecords = cartService.list(cartQuery);
        return Result.ok(cartRecords);
    }

    @RequestMapping("/changeChecked")
    public Result changeChecked(Integer id) {
        if (id == null) {
            return Result.error();
        }
        cartService.changeChecked(id);
        return Result.ok();
    }

    @RequestMapping("/setChecked")
    public Result setChecked(Integer id, Integer checked) {
        if (id == null) {
            return Result.error();
        }
        cartService.setChecked(id, checked);
        return Result.ok();
    }

    @RequestMapping("/updateCount")
    public Result updateCount(Integer id, Integer count) {
        if (id == null) {
            return Result.error();
        }
        cartService.updateCount(id, count);
        return Result.ok();
    }

    @RequestMapping("/deleteById")
    public Result  deleteById(Integer id) {
        if (id == null) {
            return Result.error();
        }
        cartService.deleteById(id);
        return Result.ok();
    }
}
