package com.sdust.mall.controller;

import com.sdust.mall.pojo.query.ProductQuery;
import com.sdust.mall.service.IProductService;
import com.sdust.mall.util.PageResult;
import com.sdust.mall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private IProductService productService;

    @RequestMapping("/list")
    public PageResult list(ProductQuery productQuery) {
        return productService.list(productQuery);
    }

    @RequestMapping("/selectById")
    public Result selectById(Integer id) {
        return Result.ok(productService.selectById(id));
    }
}
