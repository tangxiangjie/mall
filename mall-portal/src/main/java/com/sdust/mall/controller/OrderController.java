package com.sdust.mall.controller;

import com.sdust.mall.pojo.entity.Order;
import com.sdust.mall.pojo.entity.StatusInfo;
import com.sdust.mall.pojo.entity.User;
import com.sdust.mall.pojo.query.OrderQuery;
import com.sdust.mall.service.IOrderService;
import com.sdust.mall.util.PageResult;
import com.sdust.mall.util.Result;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private IOrderService orderService;

    @RequestMapping("/list")
    public PageResult list(OrderQuery query,
                           @RequestParam(value = "nameOrderNo",required = false) String queryInfo,
                           HttpSession session) {
        User user = (User) session.getAttribute("user");
        query.setUserId(user.getId());
        if (isOrderNo(queryInfo)) {
            query.setOrderNo(Long.valueOf(queryInfo));
            return orderService.selectByOrderNo(query);
        }
        query.setProductName(queryInfo);
        return orderService.list(query);
    }

    @RequestMapping("/add")
    public Result add(Order order, HttpSession session) {
        User user = (User) session.getAttribute("user");
        order.setUserId(user.getId());
        order = orderService.add(order);
        return Result.ok(order);
    }

    @RequestMapping("/changeStatus")
    public Result changeOrderStatus(StatusInfo statusInfo) {
        // 获取当前时间
        LocalDateTime currentTime = LocalDateTime.now();
        // 将 LocalDateTime 转换为 Date，如果 statusInfo 的 sendTime 是 Date 类型
        Date currentDate = Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant());
        // 如果 sendTime 是 Date 类型
        statusInfo.setEndTime(currentDate);
        orderService.updateStatus(statusInfo);
        return Result.ok();
    }

    // 判断输入是否为订单号
    private boolean isOrderNo(String str) {
        // 正则表达式用于匹配18位数字
        Pattern pattern = Pattern.compile("^\\d{18}$");
        return pattern.matcher(str).matches();
    }
}
