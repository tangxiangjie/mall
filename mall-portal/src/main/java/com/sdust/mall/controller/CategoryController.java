package com.sdust.mall.controller;

import com.sdust.mall.pojo.vo.CategoryVO;
import com.sdust.mall.service.ICategoryService;
import com.sdust.mall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private ICategoryService categoryService;
    @Autowired
    private RedisTemplate redisTemplate;

    private Map<String, List<CategoryVO>> map = new HashMap<>();

    @RequestMapping("/selectCategoryVO")
    public Result selectCategoryVO() {
        List<CategoryVO> list = categoryService.selectCategoryVO();
        return Result.ok(list);
    }

    @RequestMapping("/selectCategoryVO1")
    public Result selectCategoryVO1() {
        //1. 首先查询Redis缓存
        List<CategoryVO> list = redisTemplate.opsForList().range("categoryList", 0, -1);
        //2. Redis缓存中没有
        if (ObjectUtils.isEmpty(list)) {
            //3. 查询数据库
            list = categoryService.selectCategoryVO();
            //4. 从数据库查询之后，更新到Map缓存中
            redisTemplate.opsForList().rightPushAll("categoryList", list);
        }
        return Result.ok(list);
    }

    @RequestMapping("/selectCategoryVO2")
    public Result selectCategoryVO2() {
        //1. 首先查询Map缓存
        List<CategoryVO> list = map.get("categoryList");
        //2. Map缓存中没有
        if (ObjectUtils.isEmpty(list)) {
            //3. 查询数据库
            list = categoryService.selectCategoryVO();
            //4. 从数据库查询之后，更新到Map缓存中
            map.put("categoryList", list);
        }
        return Result.ok(list);
    }
}
