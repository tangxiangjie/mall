package com.sdust.mall.controller;

import com.sdust.mall.pojo.entity.User;
import com.sdust.mall.service.IUserService;
import com.sdust.mall.util.Result;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @RequestMapping("/checkUserLogin")
    public Result checkUserLogin(HttpSession session) {
        User user = (User) session.getAttribute("user");
        return user != null ? Result.ok() : Result.error();
    }

    @RequestMapping("/login")
    public Result login(String name, String password, HttpSession session) {
        User user = userService.login(name, password);
        if (user == null) {
            return Result.error("用户名或密码错误");
        }
        session.setAttribute("user", user);
        return Result.ok("登录成功");
    }

    @RequestMapping("/logout")
    public Result logout(HttpSession session) {
        session.removeAttribute("user");
        return Result.ok("已退出");
    }
}
