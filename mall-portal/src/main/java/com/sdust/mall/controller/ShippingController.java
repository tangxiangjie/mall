package com.sdust.mall.controller;

import com.sdust.mall.pojo.entity.Shipping;
import com.sdust.mall.pojo.entity.User;
import com.sdust.mall.pojo.query.ShippingQuery;
import com.sdust.mall.service.IShippingService;
import com.sdust.mall.util.Result;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/shipping")
public class ShippingController {
    @Autowired
    private IShippingService shippingService;

    @RequestMapping("/list")
    public Result list(ShippingQuery shippingQuery, HttpSession session) {
        User user = (User) session.getAttribute("user");
        shippingQuery.setUserId(user.getId());
        List<Shipping> shippingList = shippingService.list(shippingQuery);
        return Result.ok(shippingList);
    }

    @RequestMapping("/add")
    public Result add(@RequestBody Shipping shipping, HttpSession session) {
        User user = (User) session.getAttribute("user");
        shipping.setUserId(user.getId());
        shippingService.add(shipping);
        return Result.ok("添加成功");
    }

    @RequestMapping("/deleteById")
    public Result deleteById(Integer id) {
        shippingService.deleteById(id);
        return Result.ok("删除成功");
    }
}
