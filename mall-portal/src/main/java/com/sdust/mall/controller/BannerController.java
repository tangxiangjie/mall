package com.sdust.mall.controller;

import com.sdust.mall.pojo.entity.Banner;
import com.sdust.mall.service.IBannerService;
import com.sdust.mall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/banner")
public class BannerController {
    @Autowired
    private IBannerService bannerService;

    @RequestMapping("/list")
    public Result list() {
        List<Banner> bannerList = bannerService.list();
        return Result.ok(bannerList);
    }
}
