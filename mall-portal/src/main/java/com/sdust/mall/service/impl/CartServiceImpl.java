package com.sdust.mall.service.impl;

import com.sdust.mall.mapper.CartMapper;
import com.sdust.mall.pojo.entity.Cart;
import com.sdust.mall.pojo.query.CartQuery;
import com.sdust.mall.pojo.vo.CartVO;
import com.sdust.mall.service.ICartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartServiceImpl implements ICartService {
    @Autowired
    private CartMapper cartMapper;

    @Override
    public void add(Cart record) {
        // cartMapper.insert(record);
        Cart curRecord = cartMapper.selectCurRecord(record);
        if (curRecord == null) {
            cartMapper.insertSelective(record);
            return;
        }
        int count = curRecord.getQuantity() + record.getQuantity();
        curRecord.setQuantity(count);
        cartMapper.updateByPrimaryKeySelective(curRecord);
    }

    @Override
    public List<CartVO> list(CartQuery cartQuery) {
        return cartMapper.list(cartQuery);
    }

    @Override
    public void changeChecked(Integer id) {
        cartMapper.updateChecked(id);
    }

    @Override
    public void setChecked(Integer id, Integer checked) {
        cartMapper.setChecked(id, checked);
    }

    @Override
    public void updateCount(Integer id, Integer count) {
        cartMapper.updateCount(id, count);
    }

    @Override
    public void deleteById(Integer id) {
        cartMapper.deleteByPrimaryKey(id);
    }
}
