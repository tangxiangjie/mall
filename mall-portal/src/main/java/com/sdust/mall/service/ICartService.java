package com.sdust.mall.service;

import com.sdust.mall.pojo.entity.Cart;
import com.sdust.mall.pojo.query.CartQuery;
import com.sdust.mall.pojo.vo.CartVO;

import java.util.List;

public interface ICartService {
    void add(Cart record);

    List<CartVO> list(CartQuery cartQuery);

    void changeChecked(Integer id);

    void setChecked(Integer id, Integer checked);

    void updateCount(Integer id, Integer count);

    void deleteById(Integer id);
}
