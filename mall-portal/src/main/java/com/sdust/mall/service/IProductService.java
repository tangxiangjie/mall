package com.sdust.mall.service;

import com.sdust.mall.pojo.entity.Product;
import com.sdust.mall.pojo.query.ProductQuery;
import com.sdust.mall.util.PageResult;

public interface IProductService {
    PageResult list(ProductQuery productQuery);

    Product selectById(Integer id);
}
