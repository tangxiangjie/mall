package com.sdust.mall.service;

import com.sdust.mall.pojo.entity.User;

public interface IUserService {
    User login(String name, String password);
}
