package com.sdust.mall.service;

import com.sdust.mall.pojo.entity.Banner;

import java.util.List;

public interface IBannerService {
    List<Banner> list();
}
