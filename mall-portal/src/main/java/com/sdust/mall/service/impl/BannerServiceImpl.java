package com.sdust.mall.service.impl;

import com.sdust.mall.mapper.BannerMapper;
import com.sdust.mall.pojo.entity.Banner;
import com.sdust.mall.service.IBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BannerServiceImpl implements IBannerService {
    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public List<Banner> list() {
        return bannerMapper.listChecked();
    }
}
