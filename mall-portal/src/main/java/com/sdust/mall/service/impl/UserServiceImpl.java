package com.sdust.mall.service.impl;

import com.sdust.mall.mapper.UserMapper;
import com.sdust.mall.pojo.entity.User;
import com.sdust.mall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User login(String name, String password) {
        return userMapper.selectUser(name, password);
    }
}
