package com.sdust.mall.service;

import com.sdust.mall.pojo.vo.CategoryVO;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

public interface ICategoryService {

    List<CategoryVO> selectCategoryVO();
}
