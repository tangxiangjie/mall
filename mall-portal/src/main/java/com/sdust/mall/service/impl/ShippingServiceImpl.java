package com.sdust.mall.service.impl;

import com.sdust.mall.mapper.ShippingMapper;
import com.sdust.mall.pojo.entity.Shipping;
import com.sdust.mall.pojo.query.ShippingQuery;
import com.sdust.mall.service.IShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShippingServiceImpl implements IShippingService {
    @Autowired
    private ShippingMapper shippingMapper;


    @Override
    public List<Shipping> list(ShippingQuery shippingQuery) {
        return shippingMapper.selectByUserId(shippingQuery);
    }

    @Override
    public void add(Shipping shipping) {
        shippingMapper.insertSelective(shipping);
    }

    @Override
    public void deleteById(Integer id) {
        shippingMapper.deleteByPrimaryKey(id);
    }
}
