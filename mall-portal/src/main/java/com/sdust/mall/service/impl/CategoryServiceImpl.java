package com.sdust.mall.service.impl;

import com.sdust.mall.mapper.CategoryMapper;
import com.sdust.mall.pojo.vo.CategoryVO;
import com.sdust.mall.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements ICategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    @Cacheable(value = "categoryList")
    public List<CategoryVO> selectCategoryVO() {
        return categoryMapper.selectCategoryVO();
    }
}