package com.sdust.mall.service;

import com.sdust.mall.pojo.entity.Order;
import com.sdust.mall.pojo.entity.StatusInfo;
import com.sdust.mall.pojo.query.OrderQuery;
import com.sdust.mall.util.PageResult;

public interface IOrderService {
    Order add(Order order);

    PageResult list(OrderQuery query);

    PageResult selectByOrderNo(OrderQuery query);

    void updateStatus(StatusInfo statusInfo);
}
