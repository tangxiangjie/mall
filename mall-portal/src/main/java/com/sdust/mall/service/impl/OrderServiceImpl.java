package com.sdust.mall.service.impl;

import com.sdust.mall.mapper.CartMapper;
import com.sdust.mall.mapper.OrderItemMapper;
import com.sdust.mall.mapper.OrderMapper;
import com.sdust.mall.pojo.entity.Order;
import com.sdust.mall.pojo.entity.OrderItem;
import com.sdust.mall.pojo.entity.StatusInfo;
import com.sdust.mall.pojo.query.CartQuery;
import com.sdust.mall.pojo.query.OrderQuery;
import com.sdust.mall.pojo.vo.CartVO;
import com.sdust.mall.pojo.vo.OrderVO;
import com.sdust.mall.service.IOrderService;
import com.sdust.mall.util.SnowFlake;
import com.sdust.mall.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderItemMapper orderItemMapper;
    @Autowired
    private CartMapper cartMapper;
    @Autowired
    private SnowFlake snowFlake;

    @Override
    public Order add(Order order) {
        // 使用雪花算法 生成主键
        long orderNo = snowFlake.nextId();
        order.setOrderNo(orderNo);
        // 查询用户购物车中勾选的商品
        CartQuery cartQuery = new CartQuery();
        cartQuery.setUserId(order.getUserId());
        cartQuery.setChecked(1);
        List<CartVO> cartVOList = cartMapper.list(cartQuery);
        // 订单总金额
        BigDecimal orderTotalPrice = BigDecimal.valueOf(0.0);
        //
        for (CartVO cartVO : cartVOList) {
            OrderItem orderItem = new OrderItem();
            orderItem.setOrderNo(orderNo);
            orderItem.setUserId(cartVO.getUserId());
            orderItem.setProductId(cartVO.getProductId());
            orderItem.setProductName(cartVO.getProductName());
            orderItem.setProductImage(cartVO.getProductMainImage());
            orderItem.setUserId(cartVO.getUserId());
            orderItem.setCurrentUnitPrice(cartVO.getProductPrice());
            orderItem.setQuantity(cartVO.getQuantity());
            // orderItem.setQuantity(cartVO.getProductPrice() * cartVO.getQuantity());

            // 计算当前商品的单价 * 数量
            BigDecimal productPrice = cartVO.getProductPrice();
            BigDecimal count = BigDecimal.valueOf(cartVO.getQuantity());
            BigDecimal itemTotalPrice = productPrice.multiply(count);
            orderItem.setTotalPrice(itemTotalPrice);
            orderTotalPrice = orderTotalPrice.add(itemTotalPrice);

            orderItemMapper.insertSelective(orderItem);
            //生成订单后 清空购物车
            cartMapper.deleteByPrimaryKey(cartVO.getId());
        }
        order.setPayment(orderTotalPrice);
        orderMapper.insertSelective(order);
        return order;
    }

    @Override
    public PageResult list(OrderQuery query) {
        // PageHelper.startPage(query.getPage(), query.getLimit());
        Integer pageNo = query.getPage();
        Integer limit = query.getLimit();
        int offset = (pageNo - 1) * limit;
        query.setOffset(offset);
        orderMapper.createTempTable(query);
        List<OrderVO> list = orderMapper.list(query);
        orderMapper.deleteTempTable();
        // PageInfo pageInfo = new PageInfo<>(list);
        // int totalCount = (int) pageInfo.getTotal();
        int totalCount = orderMapper.selectCount(query);//带着条件
        return PageResult.ok(totalCount, list);
    }

    @Override
    public PageResult selectByOrderNo(OrderQuery query) {
        List<OrderVO> list = orderMapper.selectByOrderNo(query);
        return PageResult.ok(list);
    }

    @Override
    public void updateStatus(StatusInfo statusInfo) {
        orderMapper.updateStatus(statusInfo);
    }
}
