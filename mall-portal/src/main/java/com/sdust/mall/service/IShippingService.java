package com.sdust.mall.service;

import com.sdust.mall.pojo.entity.Shipping;
import com.sdust.mall.pojo.query.ShippingQuery;

import java.util.List;

public interface IShippingService {
    List<Shipping> list(ShippingQuery shippingQuery);

    void add(Shipping shipping);

    void deleteById(Integer id);
}
