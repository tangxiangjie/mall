package com.sdust.mall;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

// @ComponentScan(basePackages = {"com.sdust.util", "com.sdust.mall"})
// @SpringBootApplication (scanBasePackages = {"com.sdust.mall", "com.sdust.util", "com.sdust.config"})
@SpringBootApplication
@MapperScan("com.sdust.mall.mapper")
@EnableCaching
public class PortalApplication {

    public static void main(String[] args) {
        SpringApplication.run(PortalApplication.class, args);
    }

}
