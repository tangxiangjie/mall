package com.sdust.mall.recommend;

import com.sdust.mall.pojo.entity.RecOrder;
import org.apache.mahout.cf.taste.impl.common.FastByIDMap;
import org.apache.mahout.cf.taste.impl.model.GenericDataModel;
import org.apache.mahout.cf.taste.impl.model.GenericPreference;
import org.apache.mahout.cf.taste.impl.model.GenericUserPreferenceArray;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.model.Preference;
import org.apache.mahout.cf.taste.model.PreferenceArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MahoutDataModelBuilder {

    public static DataModel buildDataModel(List<RecOrder> orders) {
        // 使用 FastByIDMap 存储用户偏好数据，key 是用户ID，value 是用户的 PreferenceArray
        FastByIDMap<PreferenceArray> userPreferences = new FastByIDMap<>();

        // 临时存储用户的偏好列表
        Map<Long, List<Preference>> tempUserPreferences = new HashMap<>();

        // 遍历订单，填充用户的偏好列表
        for (RecOrder order : orders) {
            long userId = order.getUserId();
            long productId = order.getProductId();
            float score = (float) order.getPurchaseCount(); // 将购买次数作为评分

            tempUserPreferences.putIfAbsent(userId, new ArrayList<>());
            tempUserPreferences.get(userId).add(new GenericPreference(userId, productId, score));
        }

        // 将临时的偏好列表转换为 Mahout 需要的格式
        for (Map.Entry<Long, List<Preference>> entry : tempUserPreferences.entrySet()) {
            long userId = entry.getKey();
            List<Preference> preferences = entry.getValue();
            // 创建一个 GenericUserPreferenceArray 对象
            GenericUserPreferenceArray preferenceArray = new GenericUserPreferenceArray(preferences.size());
            for (int i = 0; i < preferences.size(); i++) {
                preferenceArray.set(i, preferences.get(i));
            }
            // 添加到 FastByIDMap
            userPreferences.put(userId, preferenceArray);
        }

        // 创建 GenericDataModel
        return new GenericDataModel(userPreferences);
    }
}
