package com.sdust.mall.recommend;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RecommenderController {

    @Autowired
    private RecommenderService recommenderService;

    @GetMapping("/recommend")
    public List<RecommendedItem> recommend(@RequestParam Long userId, @RequestParam int num) {
        try {
            List<RecommendedItem> recommendedItems = recommenderService.recommendProducts(userId, num);
            for (RecommendedItem recommendedItem : recommendedItems) {
                System.out.println(recommendedItem.getItemID() + " : " + recommendedItem.getValue());
            }
            return recommendedItems;
        } catch (TasteException e) {
            e.printStackTrace();
            return null;
        }
    }
}
