package com.sdust.mall.recommend;

import com.sdust.mall.mapper.OrderMapper;
import com.sdust.mall.pojo.entity.RecOrder;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.LogLikelihoodSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecommenderService {

    @Autowired
    private OrderMapper orderMapper;

    public List<RecommendedItem> recommendProducts(Long userId, int numberOfRecommendations) throws TasteException {
        // 从数据库获取订单数据
        List<RecOrder> orders = orderMapper.getAllOrders();
        for (RecOrder order : orders) {
            System.out.println(order);
        }
        // List<RecOrder> orders = new ArrayList<>();

        // 构建数据模型
        DataModel model = MahoutDataModelBuilder.buildDataModel(orders);

        // 计算商品相似度
        ItemSimilarity similarity = new LogLikelihoodSimilarity(model);

        // 构建推荐器
        GenericItemBasedRecommender recommender = new GenericItemBasedRecommender(model, similarity);

        // 生成推荐
        return recommender.recommend(userId, numberOfRecommendations);
    }
}
