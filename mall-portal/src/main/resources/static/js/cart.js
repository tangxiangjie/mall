$(function(){
    //初始化总价, 总选择数, 总条数;
    doPrice();
    //全选/选择框的鼠标移上变个颜色
    $('.fa-check').mouseover(function(){
        if($(this).attr('class')!='fa fa-check checked') {
            $(this).css('color', '#ff6a00');
        }
    })
    $('.fa-check').mouseleave(function(){
        $(this).css('color','#fff');
    })

    //普通勾选 —— 事件委托
    $('#cartParentNode').on('click', '.list-item .fa-check', function (){
        var cla=$(this).attr('class');
        if(cla!='fa fa-check checked'){
            $(this).attr('class','fa fa-check checked');
        }else{
            $(this).attr('class','fa fa-check');
        }
        var id = $(this).parents('.list-item').attr('id');
        changeDBChecked(id);
    });
    //普通勾选
    /*$('.fa-check').click(function(){
        var cla=$(this).attr('class');
        if(cla!='fa fa-check checked'){
            $(this).attr('class','fa fa-check checked');
        }else{
            $(this).attr('class','fa fa-check');
        }
        doCheckAll();
        doPrice();
    })*/

    //全选框勾选
    $('#check-all').click(function () {
        var cla=$(this).attr('class'); //全选框状态
        // console.log(cla);
        //
        if(cla!='fa fa-check checked'){
            $('.fa-check').attr('class','fa fa-check checked'); //购物车 单项商品 勾选
            $('.list-item .fa-check').each(function () {             //改变数据库
                var id = $(this).parents('.list-item').attr('id');
                // console.log("here is child" + id);
                setDBChecked(id, 1);
            });
        }else{
            $('.fa-check').attr('class','fa fa-check');
            $('.list-item .fa-check').each(function () {             //改变数据库
                var id = $(this).parents('.list-item').attr('id');
                // console.log("here is child" + id);
                setDBChecked(id, 0);
            });
        }
        doPrice();
    })

    //检查是否全选
      function doCheckAll(){
        var allitem=$('.list-item i[class*="fa-check"]').length;
        var checkeditem=$('.list-item i[class$="checked"]').length;
        // console.log(allitem);
        // console.log(checkeditem);
        if(allitem!=checkeditem){
            $('#check-all').attr('class','fa fa-check');
        }else{
            $('#check-all').attr('class','fa fa-check checked');
        }
    }

    //加减按钮 —— 事件委托
    $('#cartParentNode').on('click', 'button.minus', function () {
        var nowvalue=$(this).siblings('input').val();
        nowvalue=parseInt(nowvalue);
        var currentValue= 0;
        nowvalue<=1?currentValue=1:currentValue=nowvalue-1;
        $(this).siblings('input').val(currentValue);
        var id = $(this).parents('.list-item').attr('id');
        if (nowvalue !== currentValue) {
            updateDBCount(id, currentValue).then(success => {
                if (success) {
                    //计算当前的小计
                    var danjia=parseFloat($(this).parents('.good-num').siblings('.good-price').html());
                    var xiaoji=danjia*currentValue;
                    $(this).parents('.good-num').siblings('.good-total-price').html(xiaoji.toFixed(2)+'元');

                    //更新总价
                    doPrice();
                }
            })
        }
    });

    $('#cartParentNode').on('click', 'button.plus', function () {
        var nowvalue=$(this).siblings('input').val();
        nowvalue=parseInt(nowvalue);
        var currentValue=nowvalue+1;
        $(this).siblings('input').val(currentValue);
        var id = $(this).parents('.list-item').attr('id');
        if (nowvalue !== currentValue) {
            updateDBCount(id, currentValue).then(success => {
                if (success) {
                    //计算当前的小计
                    var danjia=parseFloat($(this).parents('.good-num').siblings('.good-price').html());
                    var xiaoji=danjia*currentValue;
                    $(this).parents('.good-num').siblings('.good-total-price').html(xiaoji.toFixed(2)+'元');

                    //更新总价
                    doPrice();
                }
            })
        }
    });

    // --------------- 删除对话框要绑定的事件 ----------------------

    // 绑定确认按钮的点击事件
    $('#confirmDelete').on('click', function() {
        // 从确认框的data属性中获取id
        var deleteItemId = $('.pop-del-confirm').data('deleteItemId');
        if (deleteItemId) {
            // 执行删除操作，例如发送AJAX请求删除该项
            $.post(
                '/cart/deleteById',
                {'id': deleteItemId},
                function (result) {
                    if (result.ok) {
                        doCheckAll();
                        doPrice();
                    }
                },
                'json'
            );
            // console.log('Deleting item with id:', deleteItemId);
            // 隐藏确认框
            // $('.pop-del-confirm').hide();
            $('.pop-del-confirm').fadeOut();
            $('.overlay').fadeOut();
            // 实际删除逻辑
            $('#' + deleteItemId).remove();
            // 清除data属性中的id
            $('.pop-del-confirm').removeData('deleteItemId');
        }
    });

    // 绑定取消按钮的点击事件
    $('#cancelDelete').on('click', function() {
        // 隐藏确认框
        // $('.pop-del-confirm').hide();
        $('.pop-del-confirm').fadeOut();
        $('.overlay').fadeOut();
        // 清除data属性中的id
        $('.pop-del-confirm').removeData('deleteItemId');
    });

    // 绑定确认框关闭按钮的点击事件
    $('.mi-dialog__close').on('click', function() {
        // 隐藏确认框
        // $('.pop-del-confirm').hide();
        $('.pop-del-confirm').fadeOut();
        $('.overlay').fadeOut();
        // 清除data属性中的id
        $('.pop-del-confirm').removeData('deleteItemId');
    });

    // 删除 —— 事件委托
    $('#cartParentNode').on('click', '.fa-times', function () {
        var id = $(this).parents('.list-item').attr('id');
        // 将id存储在确认框的data属性中 显示确认框
        $('.pop-del-confirm').data('deleteItemId', id);
        // 显示遮罩层和确认框，并添加淡入效果
        $('.overlay').fadeIn();
        $('.pop-del-confirm').fadeIn();
    });

    function doPrice(){
        //统计所有勾选了的值;
        var items=$('.list-item i[class*="fa-check"]');
        var checkeditems=$('.list-item i[class$="checked"]').parents('.select').siblings('.good-total-price')
        var totalprice=0;
        for(var i=0;i<checkeditems.length;i++){
            totalprice+=parseFloat(checkeditems[i].innerHTML);
        }
        //改总价 - 使用 toFixed 保留两位小数
        $('.sum-price').html(totalprice.toFixed(2));
        //改选中数
        $('.select-count').html(checkeditems.length);
        //改总条数
        $('.all-count').html(items.length);
    }

    function changeDBChecked(id) {
        $.post(
            '/cart/changeChecked',
            {'id': id},
            function (result) {
                if (result.ok) {
                    doCheckAll();
                    doPrice();
                }
            },
            'json'
        );
    }

    function setDBChecked(id, checked) {
        $.post(
            '/cart/setChecked',
            {'id': id, 'checked': checked},
            function (result) {
                if (result.ok) {
                    // doCheckAll(); //setDBChecked是统一的操作  不用再检查也可
                    doPrice();
                }
            },
            'json'
        );
    }

    // Promise的解决方案
    function updateDBCount(id, count) {
        return new Promise((resolve, reject) => {
            $.post(
                '/cart/updateCount',
                {'id': id, 'count': count},
                function (result) {
                    if (result.ok) {
                        // console.log('update count success');
                        resolve(true); // 返回 true 表示成功
                    } else {
                        // console.log('update count failed');
                        resolve(false); // 返回 false 表示失败
                    }
                },
                'json'
            ).fail(function() {
                // console.log('update count failed');
                reject(false); // 返回 false 表示失败
            });
        });
    }

    // 回调
    /*function updateDBCount(id, count, callback) {
        $.post(
            '/cart/updateCount',
            {'id': id, 'count': count},
            function (result) {
                if (result.ok) {
                    console.log('update count success');
                    callback(true); // 返回 true 表示成功
                } else {
                    console.log('update count failed');
                    callback(false); // 返回 false 表示失败
                }
            },
            'json'
        ).fail(function() {
            console.log('update count failed');
            callback(false); // 返回 false 表示失败
        });
    }
    // 使用示例
    updateDBCount(1, 10, function(success) {
        if (success) {
            console.log('Count updated successfully');
        } else {
            console.log('Failed to update count');
        }
    });
    */

    // 如果需要全局可访问，可以这样做
    window.doPrice = doPrice;
    window.doCheckAll = doCheckAll;

    /*$.post(
        '/cart/list',
        function (result) {
            console.log(result.data);
            if (result.ok) {
                $(result.data).each(function () {
                    var isChecked = this.checked == 1 ? 'checked' : '';
                    var html = '';
                    html += '<div id="'+ this.id +'" class="list list-item">';
                    html += '    <div class="select"><i class="fa fa-check ' + isChecked + '"></i></div>';
                    html += '    <div class="good-img"><img src="' + this.productMainImage + '" alt=""></div>';
                    html += '    <div class="good-name">' + this.productName + '</div>';
                    html += '    <div class="good-price">' + this.productPrice + '元</div>';
                    html += '    <div class="good-num">';
                    html += '        <div class="num-input">';
                    html += '            <button class="minus">-</button>';
                    html += '            <input type="text" value="'+ this.quantity +'" class="num-value">';
                    html += '                <button class="plus">+</button>';
                    html += '        </div>';
                    html += '    </div>';
                    html += '    <div class="good-total-price">'+ (this.quantity * this.productPrice) +'元</div>';
                    html += '    <div class="operation"><i class="fa fa-times"></i></div>';
                    html += '</div>';
                    $('#cartTitle').after(html);
                });
                doCheckAll();
                doPrice();
            }
        },
        'json'
    )*/
})