$(document).ready(function() {
    initializeAddress();
    layui.use('form', function(){
        var form = layui.form;
        form.render();

        $.post(
            '/cart/list',
            {'checked': 1},
            function (result) {
                // console.log(result);
                var totalCount = 0;
                var totalPrice = 0;
                if (result.ok) {
                    $(result.data).each(function () {
                        var html = '';
                        html += '<p>';
                        html += '    <img width="30px" src="'+this.productMainImage+'"><a style="width: 250px" href="/page/product/detail?id='+this.productId+'">' +
                            ''+this.productName+ this.productSubtitle +'</a>'+'<span style="display: inline-block;text-align: center;width: 100px">'+ this.productPrice+'元 x '+ this.quantity+'</span>';
                        html += '        <span class="total-price">'+(this.productPrice*this.quantity).toFixed(2)+'元</span>';
                        html += '</p>';
                        $('#cart_list').append(html);
                        totalCount += this.quantity;
                        totalPrice += this.quantity * this.productPrice;//不能在这里toFix,否则拼接字符串会多加前导0
                    });
                    $('#totalCount').text(totalCount + '件');
                    $('#totalPrice').text(totalPrice.toFixed(2) + '元');
                    $('#realPrice').text(totalPrice.toFixed(2) + '元');
                }
            },
            'json'
        );

        $('#confirmOrder').click(function () {
            // var radio = $("input[name='paymentType']:checked").val();
            // console.log(radio);

            $.post(
                '/order/add',
                {'shippingId':$('.address-item.active').attr('id'), 'paymentType': $("input[name='paymentType']:checked").val()},
                function (result) {
                    if (result.ok) {
                        // console.log(result.data);
                        // 跳转到支付页面
                        // http://localhost:8080/alipay/pay?subject=订单付款&traceNo=123131231&totalAmount=11&returnUrl=xxx
                        // 获取当前 URL 的路径和查询字符串
                        // 获取协议部分 (如 'http:' 或 'https:')
                        var protocol = window.location.protocol;
                        // 获取域名或 IP 地址部分 (如 'localhost' 或 'example.com')
                        var hostname = window.location.hostname;
                        // 获取端口号 (如 '8080'，如果没有指定端口，则为空字符串)
                        var port = window.location.port;
                        // 获取路径部分 (如 '/path/to/page')
                        // var pathname = window.location.pathname;
                        var pathname = '/page/order/index?targetUrl=/page/order/myorder';
                        // 获取查询字符串 (如 '?query=string')
                        // var search = window.location.search;

                        // 组合成完整的 URL
                        var fullUrl = protocol + '//' + hostname;
                        // 仅当端口号不为空且不是默认端口（80 或 443）时，才添加端口号
                        if (port) {
                            fullUrl += ':' + port;
                        }
                        // 添加路径和查询字符串
                        fullUrl += pathname;
                        var payUrl = '/alipay/pay?subject=订单付款&traceNo=' + result.data.orderNo + '&totalAmount='
                            + result.data.payment + '&returnUrl=' + fullUrl;
                        window.open(payUrl);
                        mylayer.okUrl('下单成功', '/page/order/index?targetUrl=/page/order/myorder');
                    }
                },
                'json'
            );
        });
    });
    function logOut() {
        $.post(
            '/user/logout',
            function (result) {
                if (result.ok) {
                    mylayer.okMsg(result.msg);
                    toLogin();
                }
            },
            'json'
        )
    }
})