$(document).ready(function () {
    // 头部分类搜索
    $.post(
        '/category/selectCategoryVO',
        function (result) {
            if (result.ok) {
                $(result.data).each(function () {
                    var html = '';
                    html += '<li class="category-list">'
                    html += '    <a href="#" class="cat-title">'
                    html += '        '+ this.name +''
                    html += '        <em class="cat-icon iconfont">&#xe638;</em>'
                    html += '    </a>'
                    html += '    <div class="commodity clearfix">'
                    html += '        <ul class="com-ul">'
                    $(this.categories).each(function (){
                        html += '            <li class="com-li">'
                        html += '                <a href="/page/product/list?id='+ this.id +'" class="link clearfix">'
                        html += '                    <img src="'+ this.image +'" alt="" width="40" height="40">'
                        html += '                        <span class="text">'+ this.name +'</span>'
                        html += '                </a>'
                        html += '            </li>'
                    })
                    html += '        </ul>'
                    html += '    </div>'
                    html += '</li>'
                    $('#categoryList').append(html);
                });
            }
        },
        'json'
    );

    // 获取查询字符串 (如 '?query=string')
    var queryString = window.location.search;
    // 使用 URLSearchParams 解析查询字符串
    var urlParams = new URLSearchParams(queryString);
    // 获取参数的值
    var targetUrl = urlParams.get('targetUrl');
    // console.log(targetUrl);
    if (!targetUrl) {
        targetUrl = '/page/order/myinfo';
    }
    // 加载页面
    var pageInfo = {
        page: targetUrl,
    }
    loadContent(pageInfo);

    // 处理导航链接的点击事件
    $('.order_content_div .order_left li a').click(function(e) {
        e.preventDefault(); // 阻止默认的链接跳转行为

        // 获取点击链接的data-page属性值
        // var page = $(this).data('page');
        // 获取点击链接的href属性值
        var page = $(this).attr('href');

        var pageInfo = {
            page: page,
        }

        $('#order_right').empty();
        // 加载对应的页面内容
        loadContent(pageInfo);

        // 设置选中的导航项的样式
        $('.order_content_div .order_left li').removeClass('active');
        $(this).parent().addClass('active');
    });
});

// 函数：根据页面名称加载内容
function loadContent(pageInfo) {
    // 使用jQuery的load方法加载页面内容
    $('#order_right').load(pageInfo.page, function(response, status, xhr) {
        // console.log(response);
        // console.log(status);
        // console.log(xhr);
        if (pageInfo.page.startsWith('/page/order/myorder')) {
            // 创建一个新的 URL 对象来解析 targetUrl 中的查询字符串
            var targetUrlObj = new URL(window.location.origin + pageInfo.page);
            // 使用 URLSearchParams 解析 targetUrl 的查询字符串
            var targetUrlParams = new URLSearchParams(targetUrlObj.search);
            // 获取 targetUrl 中的 status 参数的值
            var initStatus = targetUrlParams.get('status');
            // console.log(initStatus);
            if (!initStatus) {
                initializeOrderList(null);
            } else {
                // 从个人信息查看订单处的跳转——修改高亮
                updateActive('"/page/order/myorder"');
                initializeOrderList(initStatus);
            }
            $('#page-title').text('首页 > 交易订单');
        } else if (pageInfo.page == '/page/order/mydetail') {
            initializeOrderDetail(pageInfo.orderNo);
            $('#page-title').text('首页 > 订单详情');
        } else if (pageInfo.page == '/page/order/myaddress') {
            initializeAddress();
            $('#page-title').text('首页 > 收货地址');
        } else if (pageInfo.page == '/page/order/myinfo') {
            updateActive('"/page/order/myinfo"');
            initializeMyInfo();
            $('#page-title').text('首页 > 个人信息');
        }
    });
}
function updateActive(hrefValue){
    // 找到带有特定href的<li>元素
    var $targetLi = $('.order_content_div .order_left li a[href='+hrefValue+']');
    // 移除其他<li>的order-list-active类
    $('.order_content_div .order_left li').removeClass('active');
    // 给找到的<li>添加order-list-active类
    $targetLi.parent().addClass('active');
}