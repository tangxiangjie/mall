function initializeMyInfo() {
    // 事件委托——处理查看订单的点击事件
    $('.toOrderList').on('click', function (e) {
        console.log('==========detail by status is click========');
        e.preventDefault(); // 阻止默认的链接跳转行为

        // 获取点击链接的href属性值
        var page = $(this).attr('href');
        var pageInfo = {
            page: page,
        }
        $('#order_right').empty();
        loadContent(pageInfo);
    });
}