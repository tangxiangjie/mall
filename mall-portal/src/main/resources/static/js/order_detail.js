// $(document).ready(function() {
function initializeOrderDetail(orderNo) {
    // console.log(orderNo);

    // var queryString = window.location.search;
    // var urlParams = new URLSearchParams(queryString);
    // var orderNo = urlParams.get("orderNo");

    $('#order-no').text('订单号：' + orderNo);

    $.post(
        '/order/list',
        {'nameOrderNo': orderNo},
        function (result) {
            if (result.code == 0) {
                fillStatus(result.data[0]);
                $(result.data[0].items).each(function () {
                    // console.log(this);
                    fillGoodsBox(this);
                });
                fillAddressPayment(result.data[0]);
            }
        },
        'json'
    );
    // fillStatus();
    // fillGoodsBox();//each遍历

    function fillStatus(order) {
        // 从后端获取订单状态码
        var orderStatus = order.status; // 例如，订单已经交易成功

        // 定义状态码文字
        var statusTitle = {
            10: '待付款',
            20: '待发货',
            30: '待收货',
            40: '待收货',
            50: '已收货'
        };
        // 定义状态码和对应的步骤id
        var statusSteps = {
            10: 'step-order',
            20: 'step-payment',
            30: 'step-preparation',
            40: 'step-outbound',
            50: 'step-success'
        };
        // 定义状态码，及其对应的时间
        var stepTime = {
            10: order.createTime,
            20: order.paymentTime,
            30: order.sendTime,
            40: order.sendTime,// '06月02日 09:25'
            50: order.endTime
        };

        // 右上角状态
        $('#order-title').text(statusTitle[orderStatus]);
        // 找到当前状态码之前的所有步骤并设置class为step-done
        var timeHtml = '';
        for (var i = 10; i <= orderStatus; i+=10) {
            timeHtml = '<div class="info">'+ stepTime[i] +'</div>';
            $('#' + statusSteps[i]).addClass('step-done').append(timeHtml);
            // $('#' + statusSteps[i]).append(timeHtml);
        }
    }
    function fillGoodsBox(item) {
        var goodsInfo = '';
        goodsInfo += '<tr class="goods-box">';
        goodsInfo += '    <td class="col col-thumb">';
        goodsInfo += '        <div class="figure figure-thumb">';
        goodsInfo += '           <a target="_blank"';
        goodsInfo += '               href="/page/product/detail?id='+item.productId+'">';
        goodsInfo += '                <img src="'+item.productImage+'" width="80" height="80" alt="">';
        goodsInfo += '            </a>';
        goodsInfo += '        </div>';
        goodsInfo += '    </td>';
        goodsInfo += '    <td colspan="3" class="col col-info">';
        goodsInfo += '        <table class="col-info-box">';
        goodsInfo += '            <tr class="goods-box-info">';
        goodsInfo += '                <td class="col col-name">';
        goodsInfo += '                    <div class="name">';
        goodsInfo += '                        <a target="_blank" href="/page/product/detail?id='+item.productId+'">';
        goodsInfo += '                            '+item.productName;
        goodsInfo += '                        </a>';
        goodsInfo += '                    </div>';
        goodsInfo += '                </td>';
        goodsInfo += '                <td class="col col-price">';
        goodsInfo += '                    <p class="price">'+item.currentUnitPrice+'元 × '+item.quantity+'</p>';
        goodsInfo += '                </td>';
        goodsInfo += '                <td class="col col-actions"></td>';
        goodsInfo += '            </tr>';
        goodsInfo += '        </table>';
        goodsInfo += '    </td>';
        goodsInfo += '</tr>';
        $('#goods-table').append(goodsInfo);
    }
    function fillAddressPayment(order) {
        var receive_info = '';
        receive_info += '<tbody>';
        receive_info += 'tr>';
        receive_info += '    <th>姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名：</th>';
        receive_info += '    <td>'+order.receiverName+'</td>';
        receive_info += '</tr>';
        receive_info += '<tr>';
        receive_info += '    <th>联系电话：</th>';
        receive_info += '    <td>'+order.receiverPhone+'</td>';
        receive_info += '</tr>';
        receive_info += '<tr>';
        receive_info += '    <th>收货地址：</th>';
        receive_info += '    <td>'+order.receiverProvince+order.receiverCity+order.receiverDistrict+order.receiverAddress+'</td>';
        receive_info += '</tr>';
        receive_info += '</tbody>';
        $('#receive_info').append(receive_info);
        // 定义付款方式及对应的文字
        var paymentType = {
            0: '货到付款',
            1: '在线支付（微信快捷支付）',
            2: '在线支付（支付宝快捷支付）',
            3: '在线支付（云闪付快捷支付）'
        };
        var payment = '';
        payment += '<tbody>';
        payment += 'tr>';
        payment += '    <th>支付方式：</th>';
        payment += '    <td>'+paymentType[order.paymentType]+'</td>';
        payment += '</tr>';
        payment += '</tbody>';
        $('#payment_type').append(payment);
        // total_price
        var totalPayment = '';
        totalPayment += '<tbody>';
        totalPayment += '<tr>';
        totalPayment += '    <th>商品总价：</th>';
        totalPayment += '    <td><span class="num">'+order.payment.toFixed(2)+'</span>元</td>';
        totalPayment += '</tr>';
        totalPayment += '<tr>';
        totalPayment += '    <th>运费：</th>';
        totalPayment += '    <td><span class="num">0</span>元</td>';
        totalPayment += '</tr>';
        totalPayment += '<tr>';
        totalPayment += '    <th class="total">实付金额：</th>';
        totalPayment += '    <td class="total">';
        totalPayment += '        <span class="num">'+order.payment.toFixed(2)+'</span>元';
        totalPayment += '    </td>';
        totalPayment += '</tr>';
        totalPayment += '</tbody>';
        $('#total_price').append(totalPayment);
    }
}
//);