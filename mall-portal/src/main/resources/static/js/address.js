// $(function(){
function initializeAddress(){
    // 查数据库中的地址
    queryAddress();
    // --------------- 收货地址选择 --------开始------------
    $('.real-address-list').on('click', '.address-item', function () {
        $('.address-item').removeClass('active'); // 移除所有item的active类
        $(this).addClass('active'); // 为点击的item添加active类
        $('.address-action').removeClass('show-action');
        $(this).find('.address-action').addClass('show-action');
    })
    // --------------- 收货地址选择 --------结束------------
    // --------------- 收货地址编辑删除 --------开始------------
    $('.real-address-list').on('click', '.editAddress', function () {
        // console.log('=======editAddress is clicked=======');
    })
    $('.real-address-list').on('click', '.deleteAddress', function () {
        // console.log('=======deleteAddress is clicked=======');
        var id = $(this).parents('.address-item').attr('id');
        // console.log(id);
        // 将id存储在确认框的data属性中 显示确认框
        $('.pop-del-confirm').data('deleteItemId', id);
        // 显示遮罩层和确认框，并添加淡入效果
        $('.overlay').fadeIn();
        $('.pop-del-confirm').fadeIn();
    })
    // 绑定确认按钮的点击事件
    $('#confirmDelete').on('click', function() {
        // 从确认框的data属性中获取id
        var deleteItemId = $('.pop-del-confirm').data('deleteItemId');
        if (deleteItemId) {
            // 执行删除操作，例如发送AJAX请求删除该项
            $.post(
                '/shipping/deleteById',
                {'id': deleteItemId},
                function (result) {
                    console.log('success');
                },
                'json'
            );
            // console.log('Deleting item with id:', deleteItemId);
            // 隐藏确认框
            // $('.pop-del-confirm').hide();
            $('.pop-del-confirm').fadeOut();
            $('.overlay').fadeOut();
            // 实际删除逻辑
            $('#' + deleteItemId).remove();
            // 清除data属性中的id
            $('.pop-del-confirm').removeData('deleteItemId');
        }
    });

    // 绑定取消按钮的点击事件
    $('#cancelDelete').on('click', function() {
        // 隐藏确认框
        // $('.pop-del-confirm').hide();
        $('.pop-del-confirm').fadeOut();
        $('.overlay').fadeOut();
        // 清除data属性中的id
        $('.pop-del-confirm').removeData('deleteItemId');
    });

    // 绑定确认框关闭按钮的点击事件
    $('.mi-dialog__close').on('click', function() {
        // 隐藏确认框
        // $('.pop-del-confirm').hide();
        $('.pop-del-confirm').fadeOut();
        $('.overlay').fadeOut();
        // 清除data属性中的id
        $('.pop-del-confirm').removeData('deleteItemId');
    });
    // --------------- 收货地址编辑删除 --------结束------------


    // --------------- 弹出添加地址  及 添加地址组件 要绑定的事件 ----------------------
    // 点击添加地址后   弹出地址添加框
    $('#open-popup').on('click', function () {
        // 显示遮罩层和地址添加，并添加淡入效果
        $('#popup').fadeIn();
    });

    // 地址添加组件——绑定取消按钮的点击事件
    $('#close-popup').on('click', function() {
        $('#popup').fadeOut();
        $('#address-form')[0].reset(); // 重置表单，清除所有输入内容
        $('.mi-input').removeClass('mi-input-focus mi-input-active');
    });

    // 地址添加组件——绑定关闭按钮的点击事件
    $('#cancel-popup').on('click', function() {
        $('#popup').fadeOut();
        $('#address-form')[0].reset(); // 重置表单，清除所有输入内容
        $('.mi-input').removeClass('mi-input-focus mi-input-active');
    });

    // 地址添加组件——绑定关闭按钮的点击事件
    $('#confirm-popup').on('click', function() {
        const formDataArray = $('#address-form').serializeArray();
        console.log(formDataArray);
        const formDataObject = {};

        $.each(formDataArray, function(_, field) {
            formDataObject[field.name] = field.value;
        });
        console.log(formDataObject);
        console.log(JSON.stringify(formDataObject));
        $.ajax({
            type: 'POST',
            url: '/shipping/add',
            contentType: 'application/json',
            data: JSON.stringify(formDataObject),
            success: function(response) {
                console.log('Success:', response);
                // 关闭表单
                $('#popup').fadeOut();
                $('#address-form')[0].reset(); // 重置表单，清除所有输入内容
                $('.mi-input').removeClass('mi-input-focus mi-input-active');
                queryAddress();
            },
            error: function(error) {
                console.error('Error:', error);
            }
        });
    });

    // --- 添加表单中的点击事件 ---
    $('#popup').on('focus', '.mi-input', function () {
        // $('.mi-input').removeClass('mi-input-focus mi-input-active');
        // 清除其他 .mi-input 元素的 mi-input-focus 类
        $('.mi-input').each(function() {
            // 如果这个 .mi-input 下的 input 有内容，则只清除 mi-input-focus 类
            if ($(this).find('.input-text').val()) {
                $(this).removeClass('mi-input-focus');
            } else {
                // 如果没有内容，清除 mi-input-focus 和 mi-input-active 类
                $(this).removeClass('mi-input-focus mi-input-active');
            }
        });
        $(this).addClass('mi-input-focus mi-input-active');
    })
    // --------------- 弹出添加地址  及 添加地址组件 要绑定的事件  结束 ----------------------
    function queryAddress() {
        $('#real-address-list').empty();
        $.post(
            '/shipping/list',
            function (result) {
                // console.log(result);
                var isFirst = true;
                var active = 'active';
                var show_action = 'show-action';
                if (result.ok) {
                    $(result.data).each(function () {
                        var html = '';
                        html += '<div id="'+ this.id +'" class="address-item '+ active +'">';
                        html += '    <div class="address-info">';
                        html += '        <div class="name">'+this.receiverName+'';
                        html += '            <span style="color: rgb(117, 117, 117);"></span>';
                        html += '        </div>';
                        html += '        <div class="tel">'+this.receiverPhone+'</div>';
                        html += '        <div class="address-con">';
                        html += '            <span>'+this.receiverProvince+'</span>';
                        html += '            <span>'+this.receiverCity+'</span>';
                        html += '            <span>'+this.receiverDistrict+'</span>';
                        html += '            <span class="info">'+this.receiverAddress+'</span>';
                        html += '        </div>';
                        html += '        <div class="address-action '+ show_action +'">';
                        html += '            <span class="editAddress">修改</span>';
                        html += '            <span class="deleteAddress">删除</span>';
                        html += '        </div>';
                        html += '    </div>';
                        html += '    <div class="address-info-solt" style="display: none;"></div>';
                        html += '</div>';
                        $('#real-address-list').append(html);
                        if (isFirst) {
                            active = '';
                            show_action = '';
                            isFirst = false;
                        }

                    });
                }
            },
            'json'
        );
    }
}
//)