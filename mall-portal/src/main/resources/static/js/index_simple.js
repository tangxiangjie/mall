// app     初始化qpp
var J_app = document.getElementById("j-app"),
    appCode = document.querySelector(".appcode");
J_app.onmouseover = function () {
    appCode.style.height = "148px";
    J_app.classList.add('active');
}
J_app.onmouseout = function () {
    appCode.style.height = "0";
    J_app.classList.remove('active');
}

// 已登录 ==> 自己写的
var site_login = document.querySelector(".site-login"),
    login_head = document.querySelector(".login-head"),
    login_body = document.querySelector(".login-body");
if (site_login) {
    site_login.addEventListener("mouseenter", function () {
        //if (!site_login.classList.contains("user-active")) {
        site_login.classList.add("user-active");
        login_head.classList.add("head-color");
        login_body.style.height = "74px";
        // }
    });

    site_login.addEventListener("mouseleave", function () {
        site_login.classList.remove("user-active");
        login_head.classList.remove("head-color");
        login_body.style.height = "0px";
    });
}
/*下面的方案 鼠标移动到了不同的子元素上，触发多次 mouseover 事件*/
/*site_login.onmouseover = function () {
    site_login.classList.add("user-active");
    login_head.classList.add("head-color");
    login_body.style.height = "99px";
}
site_login.onmouseout = function () {
    site_login.classList.remove("user-active");
    login_head.classList.remove("head-color");
    login_body.style.height = "0";
}*/

/*购物车*/
var j_menu = document.querySelector(".j-menu"),
    site_Shop = document.querySelector(".site-shop"),
    cartColor = document.querySelector(".cart");
site_Shop.onmouseover = function () {
    j_menu.style.height = "99px";
    cartColor.classList.add("cart-color");
}
site_Shop.onmouseout = function () {
    j_menu.style.height = "0";
    cartColor.classList.remove("cart-color");
}

/*搜索框*/
var searchText = document.querySelector(".search-text"),
    searchBtn = document.querySelector(".search-btn");
searchText.onfocus = function () {
    searchText.classList.add("allBorder");
    searchBtn.classList.add("allBorder");
}
searchText.onblur = function () {
    searchText.classList.remove("allBorder");
    searchBtn.classList.remove("allBorder");
}

// 鼠标悬停时显示 site-category
$(".nav-category").mouseover(function() {
    $(this).find(".site-category-white").css("display", "block");
});

// 鼠标移出时隐藏 site-category
$(".nav-category").mouseout(function() {
    $(this).find(".site-category-white").css("display", "none");
});

/*nav - js*/
var headerNavMenu = document.querySelectorAll(".header-nav-menu"),
    navItem = document.querySelectorAll(".nav-item");
for (var i = 0; i < navItem.length; i++) {
    navItem[i].index = i;
    navItem[i].onmouseover = function () {
        for (var i = 0; i < headerNavMenu.length; i++) {
            headerNavMenu[i].style.display = "none";
        }
        headerNavMenu[this.index].style.display = "block";
        headerNavMenu[this.index].style.borderTop = "1px solid #e0e0e0";
        headerNavMenu[this.index].classList.add("menuHeight");
    }
    navItem[i].onmouseout = function () {
        headerNavMenu[this.index].classList.remove("menuHeight");
        headerNavMenu[this.index].style.border = "none";
    }
}

// 全部商品分类 --- 事件委托
$('#categoryList').on('mouseover', '.category-list', function () {
    //拿到所有的二级分类
    var commodity = document.querySelectorAll(".commodity");
    for (var i = 0; i < commodity.length; i++) {
        commodity[i].style.display = "none";
    }
    //当前一级分类下的二级分类
    // this本身就是jsdom对象
    // $(this).find('commodity')[0].style.display = 'block';
    $(this).find('.commodity').css('display', 'block');
    this.classList.add("listBcolor");
})

// 全部商品分类 --- 事件委托
$('#categoryList').on('mouseout', '.category-list', function () {
    $(this).find('.commodity').css('display', 'none');
    this.classList.remove("listBcolor");
})

//回顶部
var J_atop = document.getElementById("J_atop");
// 当网页向下滑动 854px 出现"返回顶部" 按钮
window.onscroll = function () {
    scrollFun()
};

function scrollFun() {
    if (document.body.scrollTop > 854 || document.documentElement.scrollTop > 854) {
        J_atop.classList.add("active");
    } else {
        J_atop.classList.remove("active");
    }
}

//这里定义返回底部的方法
function topFun() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

//点击返回顶部
J_atop.onclick = function () {
    topFun();
}

// 跳转到登录界面 并拼接 将当前页面的地址作为returnUri
function toLogin() {
    // 获取当前 URL 的路径和查询字符串
    var path = window.location.pathname;
    var queryString = window.location.search;

    // 拼接路径和查询字符串
    var pathAndQuery = path + queryString;

    // 输出结果
    // console.log("Path:", path);
    // console.log("Path and Query:", pathAndQuery);

    location.href = '/page/login?returnUri=' + pathAndQuery;
}

function logOut() {
    $.post(
        '/user/logout',
        function (result) {
            if (result.ok) {
                mylayer.okMsg(result.msg);
                toLogin();
            }
        },
        'json'
    )
}