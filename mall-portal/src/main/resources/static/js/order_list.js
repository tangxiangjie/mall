// $(document).ready(function() {
function initializeOrderList(initStatus) {
    layui.use(['form'], function () {
        var form = layui.form;

        var laypage = layui.laypage;
        var pageNo = 1;
        var pageSize = 3;
        var isFirst = true;
        // var name = '';
        var nameOrderNo = '';
        // var status = null;

        // 假设这是你获得的status值
        var status = initStatus;
        if (status) {
            // 找到带有特定data-value的<li>元素
            var $targetLi = $('.order_right_form li[data-value="' + status + '"]');
            // 移除其他<li>的order-list-active类
            $('.order_right_form li').removeClass('order-list-active');
            // 给找到的<li>添加order-list-active类
            $targetLi.addClass('order-list-active');
        }

        // 定义状态码文字
        var statusTitle = {
            10: '待付款',
            20: '待发货',
            30: '待收货',
            40: '待收货',
            50: '已收货'
        };
        // 查询订单
        loadData();

        // 搜索提交
        form.on('submit(submitSearch)', function(data){
            var field = data.field; // 获得表单字段JSON格式
            console.log(field);
            // 执行搜索重载
            isFirst = true;
            // name = field.productName;
            nameOrderNo = field.nameOrderNo
            loadData();
            return false; // 阻止默认 form 跳转
        });

        function loadData() {
            $.post(
                '/order/list',
                {'page': pageNo, 'limit': pageSize, 'nameOrderNo': nameOrderNo, 'status': status},
                function (result) {
                    if (result.code === 0) {
                        if(isFirst) {
                            laypage.render({
                                elem: 'layPage',
                                count: result.count, // 数据总数，从后端得到
                                theme: '#ff6802',
                                limit: pageSize,  //每页显示的条数 默认就是10
                                limits: [3,5,10,15,20], //可选的每页显示的条数
                                layout: ['prev', 'page', 'next', 'limit'],
                                jump: function(obj, first){ // ====点完页码后执行的动作,很原理====
                                    //console.log(obj.curr); // 得到当前页，以便向服务端请求对应页的数据。
                                    //console.log(obj.limit); // 得到每页显示的条数
                                    pageNo = obj.curr;
                                    pageSize = obj.limit;
                                    // 首次不执行
                                    if(!first){
                                        loadData()
                                    }
                                }
                            });
                            isFirst = false;
                        }
                        $('#order_list').empty();
                        $(result.data).each(function () {
                            var html = '';
                            html += '<div class="order_right_content">';
                            html += '    <h2>'+ statusTitle[this.status] +'</h2>';
                            html += '    <p>'+ this.createTime +' | ' + this.receiverName + ' | 订单号： '+this.orderNo+
                                ' | 在线支付 <span>订单金额：<b> '+ this.payment.toFixed(2) +'</b> 元 </span>';
                            html += '    </p>';
                            html += '    <ul style="margin: 20px 20px">';
                            var isFirstItem = true;
                            var that = this;
                            var options = getOptHtml(this);
                            $(this.items).each(function () {
                                html += '        <li>';
                                html += '            <div class="order_right_shop">';
                                html += '                <img width="80px" src="'+this.productImage+'">';
                                html += '                    <p >'+this.productName+'<br>'+this.currentUnitPrice +'元 X '+this.quantity+'</p>';
                                if (isFirstItem) {
                                    html += options;
                                }
                                html += '                    <div class="clear"></div>';
                                html += '            </div>';
                                html += '        </li>';
                                isFirstItem = false;
                            })
                            html += '    </ul>';
                            html += '</div>';
                            html += '<div class="clear"></div>';
                            $('#order_list').append(html);
                        });
                    }
                },
                'json'
            );
        }

        // 事件委托——订单类型点击事件
        $('.order_right_form ul li a').on('click', function() {
        // $('#order_right').on('click', '.order_right_form ul li a', function() {
            // 移除所有 <li> 标签上的 order-list-active 类
            $('.order_right_form ul li').removeClass('order-list-active');
            // 为当前点击的 <a> 标签的父 <li> 添加 order-list-active 类
            // $(this).parent().addClass('order-list-active');
            var $parentLi = $(this).closest('li');
            $parentLi.addClass('order-list-active');
            // 获取当前点击的 <li> 的 data-value 属性值
            status = $parentLi.data('value');
            isFirst = true;
            nameOrderNo = '';
            loadData();
            // status = null;
        });

        // 事件委托——确认收货
        $('#order_list').on('click', '.confirmReceive', function () {
        // $('#order_right').on('click', '.confirmReceive', function () {
            // console.log('confirmReceive');
            // 找到包含订单信息的父级元素
            var orderElement = $(this).closest('.order_right_content');
            // 提取订单编号
            var orderNo = orderElement.find('p').text().match(/订单号：\s*(\S+)/)[1];
            var targetUrl = '/order/changeStatus';
            // console.log(targetUrl);
            layer.confirm(
                '是否确认收货?',
                {icon : 3},
                function(index){
                    $.post(
                        targetUrl,
                        {'orderNo': orderNo, 'status': 50},
                        function (result) {
                            //console.log(result);
                            if (result.code === 0) {
                                mylayer.okMsg('已收货');
                                //删除成功之后刷新，展示最新数据
                                loadData();
                            }
                        },
                        'json'
                    );
                }
            );
        });

        // 事件委托——处理订单详情的点击事件
        $('#order_list').on('click', '.order_right_div .to-detail', function (e) {
            console.log('==========detail is click========');
            e.preventDefault(); // 阻止默认的链接跳转行为

            // 获取点击链接的href属性值
            var page = $(this).attr('href');
            if (page == '/page/order/mydetail') {
                var orderNo = $(this).data('order-no');
            }
            // 加载对应的页面内容
            var pageInfo = {
                page: page,
                orderNo: orderNo
            }
            $('#order_right').empty();
            loadContent(pageInfo);
        });

        function getOptHtml(orderInfo) {
            var options = "";
            switch(orderInfo.status) {
                case 10:
                    // 获取协议部分 (如 'http:' 或 'https:')
                    var protocol = window.location.protocol;
                    // 获取域名或 IP 地址部分 (如 'localhost' 或 'example.com')
                    var hostname = window.location.hostname;
                    // 获取端口号 (如 '8080'，如果没有指定端口，则为空字符串)
                    var port = window.location.port;
                    // 获取路径部分 (如 '/path/to/page')
                    // var pathname = window.location.pathname;
                    var pathname = '/page/order/list';

                    // 组合成完整的 URL
                    var fullUrl = protocol + '//' + hostname;
                    // 仅当端口号不为空且不是默认端口（80 或 443）时，才添加端口号
                    if (port) {
                        fullUrl += ':' + port;
                    }
                    var payUrl = '/alipay/pay?subject=订单付款&traceNo=' + orderInfo.orderNo + '&totalAmount='
                        + orderInfo.payment + '&returnUrl=' + fullUrl;
                    // 添加路径和查询字符串
                    fullUrl += pathname;
                    options += '                    <div class="order_right_div">';
                    options += '                        <a href="'+ payUrl +'" target="_blank"><p>去付款</p></a>';
                    options += '                    </div>';
                    break;
                case 20:
                    options += '                    <div class="order_right_div">';
                    options += '                        <a href="javascript: void(0)"><p>催发货</p></a>';
                    options += '                    </div>';
                    break;
                case 30:
                    options += '                    <div class="order_right_div">';
                    options += '                        <a href="javascript: void(0)" class="confirmReceive"><p>确认收货</p></a>';
                    options += '                    </div>';
                    break;
                case 40:
                    options += '                    <div class="order_right_div">';
                    options += '                        <a href="javascript: void(0)" class="confirmReceive"><p>确认收货</p></a>';
                    options += '                    </div>';
                    break;
                case 50:
                    options += '                    <div class="order_right_div">';
                    // options += '                        <a href="/page/order/detail?orderNo='+orderInfo.orderNo+'"><p>订单详情</p></a>';
                    options += '                        <a class="to-detail" href="/page/order/mydetail" data-order-no="'+orderInfo.orderNo+'"><p>订单详情</p></a>';
                    options += '                        <a href="javascript:void(0)"><p>申请售后</p></a>';
                    options += '                    </div>';
                    break;
            }
            return options;
        }
    });
}
// )