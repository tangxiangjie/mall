package com.sdust.mall.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sdust.mall.pojo.entity.OrderItem;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class OrderVO {
    // @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long orderNo;

    private Integer userId;

    private String receiverName;

    private String receiverPhone;

    private String receiverProvince;

    private String receiverCity;

    private String receiverDistrict;

    private String receiverAddress;

    private BigDecimal payment;

    private Integer paymentType;

    private Integer postage;

    private Date paymentTime;

    private Date sendTime;

    private Date endTime;

    private Date closeTime;

    private Integer status;

    private Date createTime;

    private List<OrderItem> items;
}
