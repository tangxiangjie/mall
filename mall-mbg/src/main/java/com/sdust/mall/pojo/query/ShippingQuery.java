package com.sdust.mall.pojo.query;

import lombok.Data;

@Data
public class ShippingQuery {
    private Integer page;
    private Integer limit;
    private Integer userId;
}
