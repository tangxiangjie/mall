package com.sdust.mall.pojo.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CartVO {
    private Integer id;

    private Integer userId;

    private Integer productId;
    private String productName;
    private String productSubtitle;
    private String productMainImage;
    private BigDecimal productPrice;

    private Integer quantity;

    private Integer checked;
}
