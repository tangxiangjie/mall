package com.sdust.mall.pojo.query;

import lombok.Data;

import java.util.Date;

@Data
public class ProductQuery {
    private Integer page;
    private Integer limit;
    private String name;
    private Integer categoryId;
    private Date beginCreateTime;
    private Date endCreateTime;
}
