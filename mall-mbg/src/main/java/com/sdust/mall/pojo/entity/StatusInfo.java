package com.sdust.mall.pojo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class StatusInfo {
    private Long orderNo;

    private Integer status;

    private String paymentTime;

    private Date sendTime;

    private Date endTime;
}
