package com.sdust.mall.pojo.vo;

import com.sdust.mall.pojo.entity.Category;
import lombok.Data;

import java.util.List;

@Data
public class CategoryVO {
    private Integer id;
    private String name;
    private List<Category> categories;
}
