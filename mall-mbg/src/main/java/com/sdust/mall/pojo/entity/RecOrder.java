package com.sdust.mall.pojo.entity;

import lombok.Data;

@Data
public class RecOrder {
    private Long userId;

    private Long productId;

    private int purchaseCount;
}
