package com.sdust.mall.pojo.query;

import lombok.Data;

import java.util.Date;

@Data
public class CategoryQuery {
    private Integer page;
    private Integer limit;
    private String name;
    private Date beginCreateTime;
    private Date endCreateTime;
}
