package com.sdust.mall.pojo.query;

import lombok.Data;

@Data
public class BannerQuery {
    private Integer page;

    private Integer limit;
}
