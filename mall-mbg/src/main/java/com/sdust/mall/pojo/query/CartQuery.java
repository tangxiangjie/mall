package com.sdust.mall.pojo.query;

import lombok.Data;

@Data
public class CartQuery {
    private Integer page;
    private Integer limit;
    private Integer userId;
    private Integer checked;
}
