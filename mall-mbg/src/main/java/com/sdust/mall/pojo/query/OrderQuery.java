package com.sdust.mall.pojo.query;

import lombok.Data;

import java.util.Date;

@Data
public class OrderQuery {
    private Integer page;

    private Integer offset;

    private Integer limit;

    private Integer userId;

    private String productName;

    private Long orderNo;

    private Integer status;

    private Date beginCreateTime;

    private Date endCreateTime;
}