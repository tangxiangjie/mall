package com.sdust.mall.mapper;

import com.sdust.mall.pojo.entity.Shipping;
import com.sdust.mall.pojo.query.ShippingQuery;

import java.util.List;

public interface ShippingMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Shipping record);

    int insertSelective(Shipping record);

    Shipping selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Shipping record);

    int updateByPrimaryKey(Shipping record);

    List<Shipping> selectByUserId(ShippingQuery shippingQuery);
}