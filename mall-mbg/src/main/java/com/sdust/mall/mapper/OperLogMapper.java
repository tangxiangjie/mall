package com.sdust.mall.mapper;

import com.sdust.mall.pojo.entity.OperLog;

public interface OperLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OperLog record);

    int insertSelective(OperLog record);

    OperLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OperLog record);

    int updateByPrimaryKeyWithBLOBs(OperLog record);

    int updateByPrimaryKey(OperLog record);
}