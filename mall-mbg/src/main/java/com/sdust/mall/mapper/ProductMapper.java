package com.sdust.mall.mapper;

import com.sdust.mall.pojo.entity.Product;
import com.sdust.mall.pojo.query.ProductQuery;

import java.util.List;

public interface ProductMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);

    List<Product> list(ProductQuery productQuery);
}