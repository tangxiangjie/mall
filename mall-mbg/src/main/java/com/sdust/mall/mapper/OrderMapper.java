package com.sdust.mall.mapper;

import com.sdust.mall.pojo.entity.Order;
import com.sdust.mall.pojo.entity.RecOrder;
import com.sdust.mall.pojo.entity.StatusInfo;
import com.sdust.mall.pojo.query.OrderQuery;
import com.sdust.mall.pojo.vo.OrderVO;

import java.util.List;

public interface OrderMapper {
    int deleteByPrimaryKey(Long orderNo);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Long orderNo);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    void createTempTable(OrderQuery query);

    List<OrderVO> list(OrderQuery query);

    int selectCount(OrderQuery query);

    void deleteTempTable();

    List<OrderVO> selectByOrderNo(OrderQuery query);

    void updateStatus(StatusInfo statusInfo);

    List<RecOrder> getAllOrders();
}