package com.sdust.mall.mapper;

import com.sdust.mall.pojo.entity.Banner;
import com.sdust.mall.pojo.query.BannerQuery;

import java.util.List;

public interface BannerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Banner record);

    int insertSelective(Banner record);

    Banner selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Banner record);

    int updateByPrimaryKey(Banner record);

    List<Banner> list(BannerQuery bannerQuery);

    List<Banner> listChecked();

    void changeChecked(Integer id);
}