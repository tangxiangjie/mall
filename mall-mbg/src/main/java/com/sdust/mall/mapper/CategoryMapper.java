package com.sdust.mall.mapper;

import com.sdust.mall.pojo.entity.Category;
import com.sdust.mall.pojo.query.CategoryQuery;
import com.sdust.mall.pojo.vo.CategoryVO;

import java.util.List;

public interface CategoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Category record);

    int insertSelective(Category record);

    Category selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);

    List<Category> list(CategoryQuery categoryQuery);

    List<Category> selectAll();

    Integer selectParentId(Integer childId);

    List<Category> selectByParentId(Integer parentId);

    List<CategoryVO> selectCategoryVO();
}