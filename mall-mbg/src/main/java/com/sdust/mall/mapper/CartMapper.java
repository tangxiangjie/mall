package com.sdust.mall.mapper;

import com.sdust.mall.pojo.entity.Cart;
import com.sdust.mall.pojo.query.CartQuery;
import com.sdust.mall.pojo.vo.CartVO;

import java.util.List;

public interface CartMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Cart record);

    int insertSelective(Cart record);

    Cart selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Cart record);

    int updateByPrimaryKey(Cart record);

    List<CartVO> list(CartQuery cartQuery);

    // 没用了
    Integer selectProductCount(Cart record);

    Cart selectCurRecord(Cart record);

    void updateChecked(Integer id);

    void setChecked(Integer id, Integer checked);

    void updateCount(Integer id, Integer count);
}