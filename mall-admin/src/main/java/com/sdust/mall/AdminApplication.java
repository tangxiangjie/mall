package com.sdust.mall;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

// 声明该类是一个SpringBoot的引导类（复合注解）
// 要添加对RedisConfig配置类(包路径)的扫描 否则往Redis写入数据会有乱码
// @SpringBootApplication (scanBasePackages = {"com.sdust.mall", "com.sdust.util", "com.sdust.config"})
@SpringBootApplication
// @MapperScan注解，扫描MyBatis Mapper接口类
@MapperScan("com.sdust.mall.mapper")
// quartz定时任务
@EnableScheduling
public class AdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }

}
