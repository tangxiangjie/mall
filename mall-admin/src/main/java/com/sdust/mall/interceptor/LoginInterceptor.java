/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/12 上午1:09
 * Modified date: 2024/6/12 上午1:09
 */

package com.sdust.mall.interceptor;

import com.sdust.mall.pojo.entity.User;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.web.servlet.HandlerInterceptor;

public class LoginInterceptor implements HandlerInterceptor {
    //JDK8之后  接口可以有默认实现(这时实现类实现接口后不"报错")
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.判断用户有没有登录
        //2.如果登录了，就放行，可以正常访问后台资源
        //3.如果没有登录，跳转到登录界面
        HttpSession session = request.getSession();
        //Object user = session.getAttribute("user");
        User user = (User) session.getAttribute("user");
        if (user == null) {
            //没有登录，跳转到登录界面
            response.sendRedirect("/page/login");
            return false;//拦截了 不要再放行了
        }
        //已经登录 放行
        return true;//对比类似过滤器的 filterChain.doFilter
    }
}
