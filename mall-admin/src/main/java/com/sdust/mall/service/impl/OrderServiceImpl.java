package com.sdust.mall.service.impl;

import com.sdust.mall.mapper.OrderMapper;
import com.sdust.mall.pojo.entity.StatusInfo;
import com.sdust.mall.pojo.query.OrderQuery;
import com.sdust.mall.pojo.vo.OrderVO;
import com.sdust.mall.service.IOrderService;
import com.sdust.mall.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public PageResult list(OrderQuery query) {
        // PageHelper.startPage(query.getPage(), query.getLimit());
        Integer pageNo = query.getPage();
        Integer limit = query.getLimit();
        int offset = (pageNo - 1) * limit;
        query.setOffset(offset);
        orderMapper.createTempTable(query);
        List<OrderVO> list = orderMapper.list(query);
        orderMapper.deleteTempTable();
        // PageInfo pageInfo = new PageInfo<>(list);
        // int totalCount = (int) pageInfo.getTotal();
        int totalCount = orderMapper.selectCount(query);//带着条件
        return PageResult.ok(totalCount, list);
    }

    @Override
    public PageResult selectByOrderNo(OrderQuery query) {
        List<OrderVO> list = orderMapper.selectByOrderNo(query);
        return PageResult.ok(list);
    }

    @Override
    public void updateStatus(StatusInfo statusInfo) {
        orderMapper.updateStatus(statusInfo);
    }
}
