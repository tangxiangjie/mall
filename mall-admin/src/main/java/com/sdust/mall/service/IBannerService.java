package com.sdust.mall.service;

import com.sdust.mall.pojo.entity.Banner;
import com.sdust.mall.pojo.query.BannerQuery;
import com.sdust.mall.util.PageResult;

public interface IBannerService {

    void add(Banner banner);

    PageResult list(BannerQuery bannerQuery);

    void changeChecked(Integer id);
}
