package com.sdust.mall.service;

import com.sdust.mall.pojo.entity.Category;
import com.sdust.mall.pojo.query.CategoryQuery;
import com.sdust.mall.util.PageResult;
import com.sdust.mall.util.Result;

import java.util.List;

public interface ICategoryService {
    PageResult list(CategoryQuery categoryQuery);

    Category selectById(Integer id);

    Result add(Category category);

    List<Category> selectAll();

    Integer selectParentId(Integer childId);

    List<Category> selectByParentId(Integer parentId);
}
