package com.sdust.mall.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sdust.mall.mapper.CategoryMapper;
import com.sdust.mall.pojo.entity.Category;
import com.sdust.mall.pojo.query.CategoryQuery;
import com.sdust.mall.service.ICategoryService;
import com.sdust.mall.util.PageResult;
import com.sdust.mall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements ICategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public PageResult list(CategoryQuery categoryQuery) {
        PageHelper.startPage(categoryQuery.getPage(), categoryQuery.getLimit());
        List<Category> list = categoryMapper.list(categoryQuery);
        PageInfo pageInfo = new PageInfo<>(list);
        int totalCount = (int) pageInfo.getTotal();
        return PageResult.ok(totalCount, list);
    }

    @Override
    public Category selectById(Integer id) {
        return categoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public Result add(Category category) {
        Integer insertId = categoryMapper.insertSelective(category);
        return Result.ok("添加成功");
    }

    @Override
    public List<Category> selectAll() {
        return categoryMapper.selectAll();
    }

    @Override
    public Integer selectParentId(Integer childId) {
        return categoryMapper.selectParentId(childId);
    }

    @Override
    public List<Category> selectByParentId(Integer parentId) {
        return categoryMapper.selectByParentId(parentId);
    }
}
