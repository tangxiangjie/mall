package com.sdust.mall.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sdust.mall.mapper.ProductMapper;
import com.sdust.mall.pojo.entity.Product;
import com.sdust.mall.pojo.query.ProductQuery;
import com.sdust.mall.service.IProductService;
import com.sdust.mall.util.PageResult;
import com.sdust.mall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements IProductService {
    @Autowired
    private ProductMapper productMapper;

    @Override
    public PageResult list(ProductQuery productQuery) {
        PageHelper.startPage(productQuery.getPage(), productQuery.getLimit());
        List<Product> list = productMapper.list(productQuery);
        PageInfo pageInfo = new PageInfo<>(list);
        int totalCount = (int) pageInfo.getTotal();
        return PageResult.ok(totalCount, list);
    }

    @Override
    public Product selectById(Integer id) {
        return productMapper.selectByPrimaryKey(id);
    }

    @Override
    public Integer add(Product product) {
        Integer insertId = productMapper.insertSelective(product);
        return insertId;
    }

    @Override
    public Result edit(Product product) {
        productMapper.updateByPrimaryKeySelective(product);
        return Result.ok("修改成功");
    }
}
