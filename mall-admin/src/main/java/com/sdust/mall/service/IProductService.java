package com.sdust.mall.service;

import com.sdust.mall.pojo.entity.Product;
import com.sdust.mall.pojo.query.ProductQuery;
import com.sdust.mall.util.PageResult;
import com.sdust.mall.util.Result;

public interface IProductService {
    PageResult list(ProductQuery productQuery);

    Product selectById(Integer id);

    Integer add(Product product);

    Result edit(Product product);
}
