package com.sdust.mall.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sdust.mall.mapper.BannerMapper;
import com.sdust.mall.pojo.entity.Banner;
import com.sdust.mall.pojo.query.BannerQuery;
import com.sdust.mall.service.IBannerService;
import com.sdust.mall.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BannerServiceImpl implements IBannerService {
    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public void add(Banner banner) {
        bannerMapper.insertSelective(banner);
    }

    @Override
    public PageResult list(BannerQuery bannerQuery) {
        PageHelper.startPage(bannerQuery.getPage(), bannerQuery.getLimit());
        List<Banner> list = bannerMapper.list(bannerQuery);
        PageInfo pageInfo = new PageInfo<>(list);
        int totalCount = (int) pageInfo.getTotal();
        return PageResult.ok(totalCount, list);
    }

    @Override
    public void changeChecked(Integer id) {
        bannerMapper.changeChecked(id);
    }
}
