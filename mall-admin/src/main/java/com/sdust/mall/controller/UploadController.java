/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/12 下午3:26
 * Modified date: 2024/6/12 下午3:26
 */

package com.sdust.mall.controller;

import com.sdust.mall.constant.RedisConstants;
import com.sdust.mall.util.AliOSSUtils;
import com.sdust.mall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequestMapping("/upload")
public class UploadController {
    @Autowired
    private RedisTemplate redisTemplate;

    // MultipartFile封装了所有图片上传信息
    @RequestMapping("/uploadImage")
    public Result upload(MultipartFile file) {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        // a.png
        String filename = file.getOriginalFilename();
        // .png
        String extension = filename.substring(filename.lastIndexOf("."));
        // f7a9f3e6805a4e81b5d27245c6c30070.png
        String newFilename = uuid + extension;
        // 1.本地上传
        /*String filePath = "D:\\mypic\\" + newFilename;
        try {
            file.transferTo(new File(filePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }*/
        // 2.阿里云OSS上传
        String url = "";
        try {
            url = AliOSSUtils.uploadFile(newFilename, file.getInputStream());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // 将上传的图片保存到Redis中
        redisTemplate.opsForSet().add(RedisConstants.UPLOAD_SET_NAME, url);

        return Result.ok("上传成功", url);
    }
}
