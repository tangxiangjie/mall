package com.sdust.mall.controller;

import com.sdust.mall.constant.RedisConstants;
import com.sdust.mall.pojo.entity.Banner;
import com.sdust.mall.pojo.query.BannerQuery;
import com.sdust.mall.service.IBannerService;
import com.sdust.mall.util.PageResult;
import com.sdust.mall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;

@RestController
@RequestMapping("/banner")
public class BannerController {
    @Autowired
    private IBannerService bannerService;
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/list")
    public PageResult list(BannerQuery bannerQuery) {
        return bannerService.list(bannerQuery);
    }

    @RequestMapping("/add")
    public Result add(Banner banner) {
        bannerService.add(banner);
        // 将提交到数据库的商品图片 保存到Redis中
        redisTemplate.opsForSet().add(RedisConstants.SUBMIT_SET_NAME, banner.getBannerImage());
        return Result.ok("添加成功");
    }

    @RequestMapping("/changeChecked")
    public Result changeChecked(Integer id) {
        bannerService.changeChecked(id);
        return Result.ok("已修改");
    }
}
