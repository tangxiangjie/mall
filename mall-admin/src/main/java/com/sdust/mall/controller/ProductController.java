package com.sdust.mall.controller;

import com.sdust.mall.constant.RedisConstants;
import com.sdust.mall.pojo.entity.Category;
import com.sdust.mall.pojo.entity.Product;
import com.sdust.mall.pojo.query.ProductQuery;
import com.sdust.mall.service.ICategoryService;
import com.sdust.mall.service.IProductService;
import com.sdust.mall.util.PageResult;
import com.sdust.mall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private IProductService productService;
    @Autowired
    private ICategoryService categoryService;
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/list")
    public PageResult list(ProductQuery productQuery) {
        return productService.list(productQuery);
    }

    @RequestMapping("/add")
    public Result list(Product product) {
        productService.add(product);

        // 将提交到数据库的商品图片 保存到Redis中
        redisTemplate.opsForSet().add(RedisConstants.SUBMIT_SET_NAME, product.getMainImage());

        return Result.ok("添加成功");
    }

    @RequestMapping("/editSelectById")
    public Result editById(Integer id) {
        Product product = productService.selectById(id);
        List<Category> parentCategories = categoryService.selectByParentId(0);
        Integer parentId = categoryService.selectParentId(product.getCategoryId());
        List<Category> childCategories = categoryService.selectByParentId(parentId);
        Map<String, Object> map = new HashMap<>();//用map 模拟封装一个 VO类
        map.put("product", product);
        map.put("parentId", parentId);
        map.put("parentCategories", parentCategories);
        map.put("childCategories", childCategories);
        return Result.ok(map);
    }

    @RequestMapping("/edit")
    public Result edit(Product product) {
        return productService.edit(product);
    }

    @RequestMapping("/deleteById")
    public Result deleteById(Integer id) {
        //删除逻辑 也要删除OSS存储的图片
        //先查出来 再删除
        return Result.ok();
    }
}
