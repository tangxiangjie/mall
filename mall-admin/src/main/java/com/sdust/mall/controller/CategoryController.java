package com.sdust.mall.controller;

import com.sdust.mall.pojo.entity.Category;
import com.sdust.mall.pojo.query.CategoryQuery;
import com.sdust.mall.service.ICategoryService;
import com.sdust.mall.util.PageResult;
import com.sdust.mall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private ICategoryService categoryService;

    @RequestMapping("/list")
    public PageResult list(CategoryQuery categoryQuery) {
        return categoryService.list(categoryQuery);
    }

    @RequestMapping("/selectAll")
    public Result selectAll() {
        return Result.ok(categoryService.selectAll());
    }

    @RequestMapping("/selectParentId")
    public Integer selectParentId(Integer childId) {
        return categoryService.selectParentId(childId);
    }

    @RequestMapping("/selectByParentId")
    public Result selectByParentId(Integer parentId) {
        return Result.ok(categoryService.selectByParentId(parentId));
    }

    @RequestMapping("/add")
    public Result list(Category category) {
        return categoryService.add(category);
    }
}
