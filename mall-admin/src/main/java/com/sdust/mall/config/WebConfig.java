/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/12 上午1:17
 * Modified date: 2024/6/12 上午1:17
 */

package com.sdust.mall.config;

import com.sdust.mall.converter.String2DateConverter;
import com.sdust.mall.interceptor.LoginInterceptor;
import jakarta.servlet.MultipartConfigElement;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.util.unit.DataSize;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// @Component    <bean>
// @Controller  @Service @Repository
// 这四个注解的作用是一样的，下面三个不一样主要是为了区分不同层

// @Configuration用于定义配置类，可以替换xml配置文件，
// 加了这个注解的类的内部包含一个或多个被@Bean注解的方法
@Configuration
public class WebConfig implements WebMvcConfigurer {

    //处理从request中获取到的字符串日期  String2Date
    @Configuration
    public class WebConfigurer implements WebMvcConfigurer {

        @Override
        public void addFormatters(FormatterRegistry registry) {
            registry.addConverter(new String2DateConverter() );
        }
    }

    //配置虚拟路径
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("pic/**")
                .addResourceLocations("file:/D:/mypic/");
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }

    /*<!-- 配置拦截器 in springmvc.xml -->
    <mvc:interceptors>
      <mvc:interceptor>
         <mvc:mapping path="/**"/>
         <mvc:exclude-mapping path=""/>
         <bean class="com.sdust.mvc.interceptor.MyInterceptor1"></bean>
      </mvc:interceptor>
    </mvc:interceptors>*/
    // 这个方法用来注册拦截器，我们写的拦截器需要在这里配置才能生效
    /*@Override
    public void addInterceptors(InterceptorRegistry registry) {
        //链式编程
        //把登录的拦截器配置上才能起作用
        // addPathPatterns("/**") 拦截器拦截所有的请求，静态资源也拦截了，需要放行
        // excludePathPatterns 代表哪些请求不需要拦截
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/page/login", "/user/login", "/user/logout", "/static/**", "/txj/**");
                //除了在这加"/static/**"  因为Springboot的自动配置  还要去properties里边修改static资源的pattern
                //"/txj/**"都行
                //这里是请求放行    properties配置是更改static的寻找逻辑(pattern)(相当于修改SpringBoot的自动配置)
    }*/
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //单个文件大小20Mb
        factory.setMaxFileSize(DataSize.ofMegabytes(20L));
        //设置总上传数据大小1GB
        factory.setMaxRequestSize(DataSize.ofGigabytes(1L));

        return factory.createMultipartConfig();
    }
}