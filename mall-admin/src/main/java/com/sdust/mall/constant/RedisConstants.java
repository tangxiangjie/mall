package com.sdust.mall.constant;

public class RedisConstants {
    public static final String UPLOAD_SET_NAME = "uploadImage";
    public static final String SUBMIT_SET_NAME = "submitImage";
}
