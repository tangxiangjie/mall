package com.sdust.mall.job;

import com.sdust.mall.constant.RedisConstants;
import com.sdust.mall.util.AliOSSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.ObjectUtils;

import java.util.Set;

@Configuration
public class DeleteImageJob {
    @Autowired
    private RedisTemplate redisTemplate;

    @Scheduled(cron = "0 0/2 * * * ?")
    public void deleteImage() {
        System.out.println("DeleteImageJob.deleteImage");
        // 根据Redis中保存的两个set集合进行差集运算，获得垃圾图片的名称(地址)集合
        Set<String> difference = redisTemplate.opsForSet().difference(RedisConstants.UPLOAD_SET_NAME, RedisConstants.SUBMIT_SET_NAME);
        if (!ObjectUtils.isEmpty(difference)) {
            for (String imageUrl : difference) {
                // 删除阿里云OSS中的垃圾图片
                AliOSSUtils.deleteFile(imageUrl);
                //从Redis集合中删除图片名称
                // redisTemplate.opsForSet().remove(RedisConstants.UPLOAD_SET_NAME, imageUrl);
                System.out.println("删除图片：" + imageUrl);
            }
        }
        redisTemplate.delete(RedisConstants.UPLOAD_SET_NAME);
        redisTemplate.delete(RedisConstants.SUBMIT_SET_NAME);
    }
}
